package main

import (
	"fmt"
	"gitlab.com/profick-golang/wc/wordcounter"
	"os"
)

func main() {
	args := os.Args[1:]
	if len(args) == 0 || len(args) > 2 {
		fmt.Println("Wrong number of arguments")
		printHelp()
		return
	}

	OPTION := "--words"
	var text string

	if len(args) == 1 {
		arg := args[0]
		if arg == "--help" {
			printHelp()
			return
		} else {
			text = arg
		}
	} else {
		OPTION = args[0]
		text = args[1]
	}

	var result int

	switch OPTION {
	case "--words":
		result = wordcounter.Words(text)
	case "--lines":
		result = wordcounter.Lines(text)
	case "--bytes":
		result = wordcounter.Bytes(text)
	case "--chars":
		result = wordcounter.Chars(text)
	case "--uwords":
		result = wordcounter.Uwords(text)
	default:
		fmt.Println("Illegal OPTION.")
		printHelp()
		return
	}

	fmt.Println(result)
}

func printHelp() {
	message :=
		`Usage: wc [OPTION] [TEXT]
Print newline, word, and character count in given [TEXT].
A word is a non-zero-length sequence of characters delimited by white space.
Options:
    --words
    --lines
    --bytes
    --chars
    --uwords`
	fmt.Println(message)
}
