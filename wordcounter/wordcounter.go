package wordcounter

import (
	"gitlab.com/profick-golang/util/collections"
	"gitlab.com/profick-golang/util/collections/treeset"
	"strings"
)

func Words(text string) int {
	return len(strings.Fields(text))
}

func Lines(text string) int {
	return strings.Count(text, "\n") + 1
}

func Bytes(text string) int {
	return len(text)
}

func Chars(text string) int {
	count := 0
	for range text {
		count++
	}
	return count
}

func Uwords(text string) int {
	var uwords collections.Set = treeset.NewWithStringComparator()
	words := strings.Fields(text)

	for _, word := range words {
		uwords.Add(word)
	}
	return uwords.Size()
}
