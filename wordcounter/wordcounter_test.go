package wordcounter

import "testing"

func TestBytes(t *testing.T) {
	tests := []struct {
		name string
		text string
		want int
	}{
		{"EmptyString", "", 0},
		{"OneSymbolString", "A", 1},
		{"LargeString", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", 26},
		{"MultipleWordsString", "ABCDEFGHIJ KLMNOPQRST UVWXYZ", 28},
		{"ChineseString", "世界", 6},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Bytes(tt.text); got != tt.want {
				t.Errorf("Bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestChars(t *testing.T) {
	tests := []struct {
		name string
		text string
		want int
	}{
		{"EmptyString", "", 0},
		{"OneSymbolString", "A", 1},
		{"LargeString", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", 26},
		{"MultipleWordsString", "ABCDEFGHIJ KLMNOPQRST UVWXYZ", 28},
		{"ChineseString", "世界", 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Chars(tt.text); got != tt.want {
				t.Errorf("Chars() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLines(t *testing.T) {
	tests := []struct {
		name string
		text string
		want int
	}{
		{"EmptyString", "", 1},
		{"OneSymbolString", "A", 1},
		{"LargeString", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1},
		{"MultipleWordsString", "ABCDEFGHIJ KLMNOPQRST UVWXYZ", 1},
		{"ChineseString", "世界", 1},
		{"TwoLinesString", "\n", 2},
		{"TwoLinesString2", "a\na", 2},
		{"MultipleLinesString", "\n\n\n\n\n\n\n\n\n", 10},
		{"MultipleLinesString2", "a\na\na\na\na\na\na\na\na\na", 10},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Lines(tt.text); got != tt.want {
				t.Errorf("Lines() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUwords(t *testing.T) {
	tests := []struct {
		name string
		text string
		want int
	}{
		{"EmptyString", "", 0},
		{"OneSymbolString", "A", 1},
		{"LargeString", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1},
		{"MultipleWordsString", "ABCDEFGHIJ KLMNOPQRST UVWXYZ", 3},
		{"ChineseString", "世界", 1},
		{"TwoLinesString", "\n", 0},
		{"TwoLinesString2", "a\na", 1},
		{"MultipleLinesString", "\n\n\n\n\n\n\n\n\n", 0},
		{"MultipleLinesString2", "a\na\na\na\na\na\na\na\na\na", 1},
		{"RepeatString", "one two three one two three", 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Uwords(tt.text); got != tt.want {
				t.Errorf("Uwords() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWords(t *testing.T) {
	tests := []struct {
		name string
		text string
		want int
	}{
		{"EmptyString", "", 0},
		{"OneSymbolString", "A", 1},
		{"LargeString", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1},
		{"MultipleWordsString", "ABCDEFGHIJ KLMNOPQRST UVWXYZ", 3},
		{"ChineseString", "世界", 1},
		{"TwoLinesString", "\n", 0},
		{"TwoLinesString2", "a\na", 2},
		{"MultipleLinesString", "\n\n\n\n\n\n\n\n\n", 0},
		{"MultipleLinesString2", "a\na\na\na\na\na\na\na\na\na", 10},
		{"RepeatString", "one two three one two three", 6},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Words(tt.text); got != tt.want {
				t.Errorf("Words() = %v, want %v", got, tt.want)
			}
		})
	}
}
